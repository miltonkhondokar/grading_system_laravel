<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create', function () {
    return view('grade/create');
});


Route::post('/process', function (Request $request) {
    $name = $request->name;
    $mark = $request->mark;
    $grade = '';

    if(strlen($name)>0 && strlen($mark)>0) {

        if ($mark >= 80 && $mark <= 100) {
            $grade = 'A+';
        }

        if ($mark >= 70 && $mark < 80) {
            $grade = 'A';
        }

        if ($mark >= 60 && $mark <= 70) {
            $grade = 'B';
        }

        if ($mark >= 50 && $mark < 60) {
            $grade = 'C';
        }

        if ($mark >= 40 && $mark < 50) {
            $grade = 'D';
        }

        if ($mark >= 0 && $mark < 40) {
            $grade = 'F';
        }
        if ($mark < 0 || $mark > 100) {
            echo "Negetive number or blank name";
        }
    }else{
        echo "Input some valid data";
    }

    return view('grade.process',['name'=> $name, 'mark'=>$mark, 'grade' => $grade]);
});
